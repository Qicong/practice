# -*-coding:utf-8-*-  
import requests  
import os  
import json  
import re 
from lxml import html 
import time
import glob

#防止空列表报错
def non_empty(x):
    ls = x
    if ls:
        ls = ls[0]
    else:
        ls = 'None'
    return ls
#针对生日和国籍标签内容不同
def non_empty2(x):
    ls = x
    if ls:
        ls = ls[0][3:]
    else:
        ls = 'None'
    return ls

#设计headers
sessions = requests.session()
sessions.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36' 
source='http://image.baidu.com'  
#得到vs  
vs_url=source+'/?fr=shitu'  
vs_page=sessions.get(vs_url).text  
vs_id=re.findall('window.vsid = "(.*?)"',vs_page)[0]
      
url='/pcdutu/a_upload?fr=html5&target=pcSearchImage&needJson=true'  
filepath=os.getcwd() 

pic_path = '/home/qicong/image/'#图片文件夹路径
pic_list = glob.glob(os.path.join(pic_path, '*.jpeg'))  
for pic in pic_list: 
    start = time.time()
    print(pic) 
    files={'file':(pic,open(pic,'rb'),'image/jpeg'),'pos':(None,'upload'),  
           'uptype':(None,'upload_pc'),'fm':(None,'home')}  
    r=sessions.post(source+url,files=files)

    tmp=r.text  
    tmp_json=json.loads(tmp) 
    queryImageUrl=tmp_json['url']  
    querySign=tmp_json['querySign']  
    simid=tmp_json['simid']  
    url2=source+'/pcdutu?queryImageUrl='+queryImageUrl+'&querySign='+querySign    +'fm=index&uptype=upload_pc&result=result_camera&vs='+vs_id  
    r2=sessions.get(url2).text  
    tree = html.fromstring(r2)
    #获取标签内容
    similarity = tree.xpath('/html/body/div[2]/div[1]/div[3]/div[2]/div[1]/div[2]/span/text()') #相似度
    similarity = non_empty(similarity)
    cname = tree.xpath('//*[@id="guess-newbaike-name"]/text()') #中文名
    cname = non_empty(cname)
    birthday = tree.xpath('/html/body/div[2]/div[1]/div[3]/div[2]/div[2]/div[2]/div/p[1]/span[1]/text()') #生日
    birthday = non_empty2(birthday)
    nationality = tree.xpath('/html/body/div[2]/div[1]/div[3]/div[2]/div[2]/div[2]/div/p[1]/span[2]/text()') #国籍
    nationality = non_empty2(nationality)
    all_data = tree.xpath('/html/body/script[7]/text()')[0] 
    gender = re.findall(r'"gender":"(.*?)"',all_data) #性别
    gender = non_empty(gender)
    url3='https://baike.baidu.com/item/'+str(cname)
    response=sessions.get(url3)
    response.encoding="utf-8"
    r3 = response.text
    pattern = re.compile(r'''<dt class="basicInfo-item name">外文名</dt>
<dd class="basicInfo-item value">
(.*?)
</dd>''')
    ename = re.findall(pattern,r3) #英文名
    ename = non_empty(ename)
    pattern2 = re.compile(r'''<dt class="basicInfo-item name">星&nbsp;&nbsp;&nbsp;&nbsp;座</dt>
<dd class="basicInfo-item value">
(.*?)
</dd>''')
    constellation = re.findall(pattern2,r3) #星座
    constellation = non_empty(constellation)
    pattern3 = re.compile(r'''<dt class="basicInfo-item name">血&nbsp;&nbsp;&nbsp;&nbsp;型</dt>
<dd class="basicInfo-item value">
(.*?)
</dd>''')
    blood_type = re.findall(pattern3,r3) #血型
    blood_type = non_empty(blood_type)
    end = time.time()
    print(' 相似度:',similarity,'\n','中文名:',cname,'\n','生日:',birthday,'\n','国籍:',nationality,'\n','性别:',gender,'\n','英文名:',ename,'\n','星座:',constellation,'\n','血型:',blood_type,'\n')
    print(" 用时：", (end-start),'s','\n')